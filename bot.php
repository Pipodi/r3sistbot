<?php

//$botToken = "bot"."730212910:AAHARPXaaOFJe72oR629L4dzmkJ3l4L3Jes"; FOR TESTING PURPOSES
$botToken = "bot"."743792208:AAEe8f_s46I-Ha8O5ZczEw4qEadsFBkLt74";

$TelegramRawInput = file_get_contents("php://input");

$update = json_decode($TelegramRawInput, TRUE);

$db = new SQLite3("database");
$db->enableExceptions(true);

if(!$update){
  exit;
}

$MessageObj = $update['message'];
$cmd = $MessageObj['text'];
$chatId = $MessageObj['chat']['id'];
saveInJsonFile($update, "ricevuto.json");


if (strpos($cmd, '/sub') === 0){
    $args = explode("\"", $cmd);

    list($title, $season, $episode) = explode(",", $args[1]);

    if (is_numeric($season) && is_numeric($episode)){
        $table_name = strtolower($title);
        $table_name = str_replace('\'', '', $table_name);
        $table_name = str_replace('&', '', $table_name);
        $table_name = str_replace(' ', '_', $table_name);
        $table_name = str_replace('__', '_', $table_name);

        try {
            $result = $db->query('select tg_id from ' . $table_name . ' where ep=' . $episode . ' and season=' . $season);
            $count = 0;
            while ($row = $result->fetchArray()) {
                $count++;
            }
            $row = $result->fetchArray();
            $tg_id = $row['tg_id'];
            if ($tg_id === "") {
                sendMsg($botToken, $chatId, $title . " " . $season . "x" . $episode . " non trovato. Riprova.");

            } else {
                $out = getFile($botToken, $chatId, $tg_id, $title, $season, $episode);
            }
        }catch (Exception $exception){
            if (strpos($exception->getMessage(), "no such table")){
                sendMsg($botToken, $chatId, "Serie tv non presente nel nostro database. Sei sicuro di aver scritto correttamente il titolo? Digita /lista per vedere le serie che stiamo traducendo.");
            }
        }


    }else {
        sendMsg($botToken, $chatId, "Parametri di ricerca errati, la sintassi corretta è \"titolo,stagione,episodio\" dopo /sub. Stagione ed episodio devono essere delle cifre.");
    }
} else if (strpos($cmd, '/lista') === 0){
    $result = $db->query('select title from series order by title asc');
    $content = "Le serie che traduciamo sono: \n";
    while ($row = $result->fetchArray()){
        $content .= "- ".$row['title']."\n";
    }
    $content .= "\n Per scaricare i relativi sottotitoli, digita /sub \"titolo,stagione,episodio\", ad esempio: /sub \"This Is Us,3,10\"";
    sendMsg($botToken, $chatId, $content);
} else if (strpos($cmd, '/start') === 0){
    $content = "Benvenuto! I comandi disponibili al momento sono: \n - /sub (per ricercare e scaricare un determinato sottotitolo); \n - /lista (per ricevere la lista delle serie che traduciamo); \n - /dettaglio (per ricevere la lista dettagliata degli episodi tradotti per una determinata serie). \n\n Digita /aiuto per ulteriori informazioni sull'uso dei comandi.";
    sendMsg($botToken, $chatId, $content);
} else if (strpos($cmd, '/aiuto') === 0){
    $content = "*GUIDA AI COMANDI*\n *Comando /sub*\n Con il comando /sub potrai scaricare sul tuo dispositivo i nostri sottotitoli di una determinata serie. Il comando è il seguente: /sub \"_titolo,stagione,episodio_\", dove _titolo_, _stagione_ ed _episodio_ devono essere sostituiti con le informazioni del sottotitolo che si vuole scaricare. \n Ad esempio, ti servono i sottotitoli di This Is Us, stagione 3, episodio 10? Se l'episodio è stato tradotto da noi ed è stato caricato sul canale ([Clicca qui per entrare nel canale](t.me/r3sist)), ti basterà digitare /sub \"This Is Us,3,10\" e in pochi secondi avrai il sottotitolo pronto al download.\n\n *Comando /lista*\nCon quuesto comando potrai vedere la lista delle serie che stiamo traducendo. Verrà aggiornata man mano che verranno prese in carico altre serie. \n\n *Comando /dettaglio* \nCon questo comando potrai vedere la lista dettagliata degli episodi tradotti per una determinata serie. Ti basta digitare /dettaglio \"_titolo_\", dove _titolo_ è il titolo della serie TV. Ad esempio: /dettaglio \"The Flash\"";
    sendMsg($botToken, $chatId, $content);
} else if (strpos($cmd, '/dettaglio') === 0) {
    $args = explode("\"", $cmd);
    if (sizeof($args) == 1) {
        sendMsg($botToken, $chatId, "Titolo serie mancante. Il comando esatto è /dettaglio \"_titolo_\"");
    } else {
        $title = $args[1];
        $table_name = strtolower($title);
        $table_name = str_replace('\'', '', $table_name);
        $table_name = str_replace('&', '', $table_name);
        $table_name = str_replace(' ', '_', $table_name);
        $table_name = str_replace('__', '_', $table_name);
        try{
            $result = $db->query('select ep, season from ' . $table_name);

            $content = "Per " . $title . " sono disponibili questi episodi: \n";
            while ($row = $result->fetchArray()) {
                $content .= "- " . $row['season'] . "x" . $row['ep'] . "\n";
            }

            $content .= "\n Per scaricare i relativi sottotitoli, digita /sub \"titolo,stagione,episodio\", ad esempio: /sub \"This Is Us,3,10\"";
            sendMsg($botToken, $chatId, $content);
        } catch (Exception $exception){
            if (strpos($exception->getMessage(), "no such table")){
                sendMsg($botToken, $chatId, "Serie tv non presente nel nostro database. Sei sicuro di aver scritto correttamente il titolo? Digita /lista per vedere le serie che stiamo traducendo.");
            }
        }

    }
} else {
    sendMsg($botToken, $chatId, "Comando non riconosciuto! Digita /aiuto per conoscere i comandi disponibili.");
}

saveInJsonFile($out, "inviato.json");

function sendMsg($tkn, $cId, $msgTxt){
    $TelegramUrlSendMessage = "https://api.telegram.org/".$tkn."/sendMessage?chat_id=".$cId."&text=".urlencode($msgTxt)."&parse_mode=Markdown";
    return file_get_contents($TelegramUrlSendMessage);
}

function saveInJsonFile($data, $filename){
    if(file_exists($filename))
        unlink($filename);
    file_put_contents($filename,json_encode($data,JSON_PRETTY_PRINT));
}

function getFile($botToken, $chatId, $tg_id, $title, $season, $episode){
    $getFileCall = 'https://api.telegram.org/'.$botToken.'/getFile?file_id='.$tg_id;
    $response = file_get_contents($getFileCall);
    $responseObj = json_decode($response, TRUE);
    $path = $responseObj['result']['file_path'];
    $content = "[".$title." ".$season."x".$episode.".zip](https://api.telegram.org/file/".$botToken."/".$path.")";
    $fileName_tg = explode('/', $path);
    sendMsg($botToken, $chatId, "*Attenzione! Il file verrà salvato sul dispositivo come ".$fileName_tg[1]. ", ma al suo interno troverai il file .srt corrispondente alla puntata, opportunamente rinominato.*");
    $TelegramUrlSendMessage = "https://api.telegram.org/".$botToken."/sendMessage?chat_id=".$chatId."&text=".$content.'&parse_mode=Markdown';
    return file_get_contents($TelegramUrlSendMessage);
}

?>