<html>
    <head>
        <title>R3sist Upload Tool</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

        <link rel="stylesheet" href="style.css"

        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <?php

    //VARIABLES
    $target_dir = "uploads/";

    $series_name = $_POST['series-picker'];
    $series_sub_ep = $_POST['series-sub-episode'];
    $series_sub_season = $_POST['series-sub-season'];
    $table_name = strtolower($series_name);
    $table_name = str_replace('\'', '', $table_name);
    $table_name = str_replace('&', '', $table_name);
    $table_name = str_replace(' ', '_', $table_name);
    $table_name = str_replace('__', '_', $table_name);
    $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
    $db = new SQLite3("database");
    $db->enableExceptions(true);
    $file_parts = pathinfo($target_file);

    //CHAT IDs
    $chat_id = -1001282299722;
    //$chat_id = -321158479; FOR TESTING PURPOSES

    //BOT TOKENS
    $bot_token = '743792208:AAEe8f_s46I-Ha8O5ZczEw4qEadsFBkLt74';
    //$bot_token = '730212910:AAHARPXaaOFJe72oR629L4dzmkJ3l4L3Jes'; FOR TESTING PURPOSES


    if ($file_parts['extension'] === "zip") {
        if (isset($_POST["submit"])) {
            $result = $db->query('select * from ' . $table_name . ' where ep=' . $series_sub_ep . ' and season=' . $series_sub_season);
            $rows = 0;
            while ($row = $result->fetchArray()) {
                $rows++;
            }
            if ($rows > 0) {
                echo '<span style="color:red;">L \'episodio ' . $series_sub_ep . 'x' . $series_sub_season . ' di ' . $series_name . ' è già stato caricato. Torna indietro e riprova.</p>';
            } else {
                if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                    $fileName = basename($_FILES["fileToUpload"]["name"]);

                    $caption = $_POST['comment'];

                    // Create a cURL handle
                    $ch = curl_init('https://api.telegram.org/bot' . $bot_token . '/sendDocument');

                    // Create a CURLFile object
                    $cfile = curl_file_create($target_file, 'application/zip', $fileName);

                    // Assign POST data
                    $data = array('document' => $cfile, 'chat_id' => $chat_id, 'caption' => $caption);
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

                    // Execute the handle
                    $response = curl_exec($ch);
                    curl_close($ch);
                    $responseObj = json_decode($response, TRUE);
                    $file_id_tg = $responseObj['result']['document']['file_id'];
                    try {
                        $result = $db->query('insert into ' . $table_name . '(ep, season, tg_id) values (' . $series_sub_ep . ', ' . $series_sub_season . ', \'' . $file_id_tg . '\')');
                        echo '<span style="color:green;">Il file ' . basename($_FILES["fileToUpload"]["name"]) . ' è stato caricato.</p>';
                    } catch (Exception $ex) {
                        $log = '[' . date('j/n/y') . '] Exception caught while adding a new series. Exception: ' . $ex->getMessage();
                        file_put_contents('logs/log_' . date('j_n_y') . 'txt', $log, FILE_APPEND);
                        mail('pipodi93@gmail.com', 'SQLite Exception R3sist', $log);
                        echo '<span style="color:red;">Errore durante il caricamento del file.</p>';
                    }
                }
            }
        }
    }else{
        echo '<span style="color:red;">Errore durante il caricamento del file. Il file da caricare deve essere un file .zip</p>';
    }

    ?>
    <input type="button" class="btn btn-primary" onclick="window.history.back();" value="Torna indietro" />

</html>