<?php
    $name = $_POST['name'];
    $db = new SQLite3("database");
    $db->enableExceptions(true);
    $table_name = strtolower($name);
    $table_name = str_replace('\'', '', $table_name);
    $table_name = str_replace('&', '', $table_name);
    $table_name = str_replace(' ', '_', $table_name);
    $table_name = str_replace('__', '_', $table_name);
    $result = $db->query('SELECT name FROM sqlite_master WHERE type=\'table\' AND name=\''.$table_name.'\';');
    $row = $result->fetchArray();
    if ($row['name'] == $table_name) {
        echo 'already_added';
    } else {
        try {
            $name = str_replace('\'', '\'\'', $name);
            $result = $db->query('insert into series (title) values (\'' . $name . '\');');
            $result = $db->query('CREATE TABLE ' . $table_name . ' (
            id	INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
            ep	INTEGER,
            season	INTEGER,
            tg_id	TEXT)');
            echo 'ok';
        } catch (Exception $e) {
            $log = '[' . date('j/n/y') . '] Exception caught while adding a new series. Exception: ' . $e->getMessage();
            file_put_contents('logs/log_' . date('j_n_y') . '.txt', $log, FILE_APPEND);
            mail('pipodi93@gmail.com', 'SQLite Exception R3sist', $log);
            echo 'ko';
        }
    }
