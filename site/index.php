<html>
<head>
    <title>R3sist Upload Tool</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <link rel="stylesheet" href="style.css"

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
<div class="container-fluid">
    <!-- Modal -->
    <div class="modal fade" id="seriesPopup" tabindex="-1" role="dialog" aria-labelledby="seriesPopupLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h class="modal-title" id="seriesPopupLabel">Aggiungi una nuova serie</h>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <label for="add-series-input">Title:</label>
                    <input class="form-control" type="text" name="add-series-input" id="add-series-input" placeholder="Titolo" required>
                    <p class="statusMsg"></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Chiudi</button>
                    <button type="submit" formaction="update_user.php" class="btn btn-success" id="add-series-btn" name="add-series-btn" onclick="addSeriesToDB()">Aggiungi</button>
                </div>
            </div>
        </div>
    </div>

    <form action="upload.php" method="post" enctype="multipart/form-data">
        <div class="form-group">
            <label for="series-picker">Seleziona la serie:</label>
            <select class="custom-select" id="series-picker" name="series-picker" required>
                <?php
                $db = new SQLite3("database");
                $result = $db->query('SELECT title FROM series ORDER BY title ASC');
                while ($row = $result->fetchArray()) {
                    $title = $row['title'];
                    echo('<option value="' .$title.'">'.$title.'</option>');
                }
                ?>
            </select>
            <button type="button" id="addSeriesButton" name="addSeriesButton" class="btn btn-success" data-toggle="modal" data-target="#seriesPopup">
                Aggiungi una serie
            </button>

            <label for="series-sub-season">Stagione:</label>
            <input class="form-control" type="number" name="series-sub-season" id="series-sub-season" required>

            <label for="add-series-input">Episodio:</label>
            <input class="form-control" type="number" name="series-sub-episode" id="series-sub-episode" required>

            <label for="fileToUpload">Seleziona il file da caricare (estensione .zip):</label>
            <input class="form-control-file" type="file" name="fileToUpload" id="fileToUpload" accept="application/zip" required>
            <label id="commentlabel" for="comment">Didascalia file:</label>
            <textarea class="form-control" type="text" name="comment" id="comment"></textarea>
            <input class="btn btn-primary" type="submit" value="Carica file" name="submit">
        </div>
    </form>
</div>
</body>
<script>
    $('#seriesPopup').on('hidden.bs.modal', function () {
        location.reload();
    });
    function addSeriesToDB(){
        var name = $('#add-series-input').val();
        if (name.trim() == ''){
            alert('Inserisci un titolo.');
            $('#add-series-input').focus();
            return false;
        } else {
            $.ajax({
                type: 'POST',
                url: 'add.php',
                data: 'addtvseries=1&name=' + name,
                beforeSend: function () {
                    $('#add-series-btn').attr("disabled","disabled");
                },
                success: function(msg) {
                    if (msg == 'ok') {
                        $('#add-series-input').val('');
                        $('.statusMsg').html('<span style="color:green;">Serie aggiunta correttamente!</p>');
                    } else if (msg == 'already_added') {
                        $('.statusMsg').html('<span style="color:red;">La serie è già presente nel database!</span>');
                    } else {
                        $('.statusMsg').html('<span style="color:red;">C\'è stato un problema durante l\'aggiunta della serie.</span>');
                    }
                    $('#add-series-btn').removeAttr("disabled");
                }
            });
        }
    }
</script>
</html>